package com.goldina.workwithrx

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.goldina.workwithrx.databinding.ActivityMainBinding
import io.reactivex.rxjava3.core.Observable

class MainActivity : AppCompatActivity(), Common {
    private lateinit var binding: ActivityMainBinding
    private val fragment1: Fragment = FirstFragment()
    val fragment2: Fragment = SecondFragment()
    var active: Fragment = fragment1
    var Observable: Observable<String> = io.reactivex.rxjava3.core.Observable.empty()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().add(R.id.fragmentContainerView, fragment2, "2").hide(fragment2).commit()
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainerView,fragment1, "1").commit()

        binding.bottomNav.setOnItemSelectedListener{
            when(it.itemId){
                R.id.first -> {
                    supportFragmentManager.beginTransaction().hide(active).show(fragment1).commit()
                    active = fragment1
                }
                R.id.second->{
                    supportFragmentManager.beginTransaction().hide(active).show(fragment2).commit()
                    active = fragment2
                }
            }
            true
        }
    }

    override fun getObservable(myObservable: Observable<String>) {
        Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show()
        Observable = myObservable
    }
}