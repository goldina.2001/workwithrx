package com.goldina.workwithrx

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import io.reactivex.rxjava3.core.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.*


const val BASE_URL="https://jsonplaceholder.typicode.com/"

class SecondFragment : Fragment(),Common {
    private lateinit var buttonUpload: Button
    private lateinit var buttonGet: Button
    private lateinit var textViewStatus: TextView
    lateinit var myObservable: Observable<String>
    internal var activity: Activity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        super.onAttach(requireContext())
        if (context is Activity) activity = context as Activity

        val value = inflater.inflate(R.layout.fragment_second, container, false)
        buttonUpload = value.findViewById(R.id.buttonUpload)
        buttonGet = value.findViewById(R.id.buttonGet)
        textViewStatus = value.findViewById(R.id.textViewStatus)
        buttonGet.setOnClickListener {
            val stream = resources.openRawResource(R.raw.data)
            val writer: Writer = StringWriter()
            val buffer = CharArray(1024)
            stream.use { stream ->
                val reader: Reader = BufferedReader(InputStreamReader(stream, "UTF-8"))
                var n: Int
                while (reader.read(buffer).also { n = it } != -1) {
                    writer.write(buffer, 0, n)
                }
            }
            val jsonString: String = writer.toString()
            val itemList = Gson().fromJson(jsonString, Array<MyDataItem>::class.java).asList()
            var stringList = ArrayList<String>()
            for (item in itemList){
                stringList.add("id: ${item.id}\ntitle: ${item.title}")
            }
             myObservable = Observable.fromIterable(stringList)

            try {
                (activity as Common).getObservable(myObservable)
             } catch (ignored: ClassCastException){ }
        }
        buttonUpload.setOnClickListener {
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            val retrofitData = retrofitBuilder.getData()
            retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<List<MyDataItem>?>,
                    response: Response<List<MyDataItem>?>,
                ) {
                    textViewStatus.text = "Данные загружены"
                }
                override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                    textViewStatus.text = "Ошибка: ${t.message}"
                }

            })
        }
        return value
    }
    override fun getObservable(myObservable: Observable<String>) {  }

}