package com.goldina.workwithrx

import io.reactivex.rxjava3.core.Observable

interface Common {
    fun getObservable(myObservable: Observable<String>)
}