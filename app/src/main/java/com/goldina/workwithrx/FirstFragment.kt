package com.goldina.workwithrx

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.util.*


class FirstFragment : Fragment(){
    val TAG = "TAG"
    private lateinit var textViewReceive: TextView
    private lateinit var buttonStart: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val activity: MainActivity = (activity as MainActivity)
        val value = inflater.inflate(R.layout.fragment_first, container, false)
        textViewReceive = value.findViewById(R.id.textViewReceive)
        buttonStart = value.findViewById(R.id.buttonStart)

        buttonStart.setOnClickListener {
            activity.Observable.subscribe(createStringSubscriber())
        }
        return value
    }

    fun createStringSubscriber(): Observer<String> {
        val mySubscriber = object: Observer<String> {
            override fun onNext(s: String) {
                runBlocking {
                        delay(500L)
                        Log.i("TAG", "onNext: $s and ${Date()} ")
                }
                textViewReceive.text = s
            }

            override fun onComplete() {
                Log.i(TAG, "onComplete: ")
                println("onComplete")
                textViewReceive.text ="Данные иссякли"
            }

            override fun onError(e: Throwable) {
                Log.i(TAG, "onError: ")
                textViewReceive.text ="Ошибка: ${e.message}"
            }

            override fun onSubscribe(s: Disposable) {
                Log.i(TAG, "onSubscribe: ")
            }
        }

        return mySubscriber
    }

}